# KalmanDataset

Der Datensatz wurde generiert für eine Seminararbeit im Fach Study Know-How an der Hochschule München.

In den Daten (500 Datenpunkte) wurde eine konstante geografische Breite "lat" mit dem Wert 11.3 und eine konstante geografische Breite "lon" mit dem Wert 48.7 mit weißem Rauschen mit Varianz 0.00001 berauscht.

Die Geschwindigkeit in Richtung "lat" und "lon" ist 0.
